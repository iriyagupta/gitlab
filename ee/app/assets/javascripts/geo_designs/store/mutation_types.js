export const SET_FILTER = 'SET_FILTER';
export const SET_SEARCH = 'SET_SEARCH';
export const SET_PAGE = 'SET_PAGE';

export const REQUEST_DESIGNS = 'REQUEST_DESIGNS';
export const RECEIVE_DESIGNS_SUCCESS = 'RECEIVE_DESIGNS_SUCCESS';
export const RECEIVE_DESIGNS_ERROR = 'RECEIVE_DESIGNS_ERROR';
